﻿using System;
using System.Net.WebSockets;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace TestMail
{
    [TestFixture]
    public class Tests
    {
        public IWebDriver WebDriver { get; set; }
        public WebDriverWait Wait { get; set; }

        [OneTimeSetUp]
        public void Setup()
        {
            WebDriver = new ChromeDriver();
            Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(30));
        }

        [OneTimeTearDown]
        public void Teardown()
        {
            WebDriver.Quit();
        }
        
        [Test]
        [TestCase("aleks.miroshnikov.test", "", "ultra164@gmail.com", "subj", "body")]
        public void Test1(string login, string password, string to, string subject, string body)
        {
            try
            {
                var mail_ru_web_page = new MailRuWebPage(WebDriver);
                mail_ru_web_page.Navigate();
                mail_ru_web_page.Login(login, password);
                System.Threading.Thread.Sleep(10 * 1000);
                mail_ru_web_page.SendEmail(to, subject, body);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}