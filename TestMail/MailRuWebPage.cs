﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.PageObjects;

namespace TestMail
{
    public class MailRuWebPage
    {
        private readonly string url = @"https://mail.ru";
        private readonly IWebDriver m_web_driver;

        #region Controls
        [FindsBy(How = How.Id, Using = "mailbox:login-input")]
        private  IWebElement LoginInput { get; set; }
        
        [FindsBy(How = How.Id, Using = "mailbox:submit-button")]
        private IWebElement LoginButton { get; set; }
        
        [FindsBy(How = How.Id, Using = "mailbox:password-input")]
        private IWebElement PasswordInput { get; set; }
        #endregion

        public MailRuWebPage(IWebDriver web_driver)
        {
            m_web_driver = web_driver;
            m_web_driver.Manage().Window.Maximize();
            PageFactory.InitElements(web_driver, this);
        }

        public void Navigate()
        {
            m_web_driver.Navigate().GoToUrl(url);
        }

        public void Login(string login, string password)
        {
            LoginInput.Clear();
            LoginInput.SendKeys(login);
            LoginButton.Click();
            PasswordInput.Clear();
            PasswordInput.SendKeys(password);
            LoginButton.Click();
        }

        public void SendEmail(string to, string subject, string body)
        {
            new Actions(m_web_driver).SendKeys("n").Perform();
            System.Threading.Thread.Sleep(5*1000);
            new Actions(m_web_driver).SendKeys(to).SendKeys(Keys.Tab).SendKeys(subject).SendKeys(Keys.Tab).SendKeys(Keys.Tab)
                .SendKeys(body).Perform();
            
            var send_button = m_web_driver.FindElement(By.XPath("//span[text()='Отправить']"));
            send_button.Click();
        }
    }
}